(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#1c1d20" "#ea3d54" "#78bd65" "#fedd38" "#4fb3d8" "#b978ab" "#4fb3d8" "#cbccd1"])
 '(custom-safe-themes
   '("4f01c1df1d203787560a67c1b295423174fd49934deb5e6789abd1e61dba9552" default))
 '(fci-rule-color "#5B6268")
 '(jdee-db-active-breakpoint-face-colors (cons "#1B2229" "#fedd38"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#1B2229" "#78bd65"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#1B2229" "#3f444a"))
 '(objed-cursor-color "#ea3d54")
 '(org-agenda-files
   '("~/org/purchase.org" "~/org/russian.org" "~/org/roam/20201230163511-hw_y1s2.org" "~/org/todo.org" "~/org/hwu_ah2001/source/todo.org" "/home/bond7o6/org/computerSetup.org"))
 '(pdf-view-midnight-colors (cons "#cbccd1" "#1c1d20"))
 '(rustic-ansi-faces
   ["#1c1d20" "#ea3d54" "#78bd65" "#fedd38" "#4fb3d8" "#b978ab" "#4fb3d8" "#cbccd1"])
 '(safe-local-variable-values '((git-commit-major-mode . git-commit-elisp-text-mode)))
 '(smtpmail-smtp-server "outlook.office365.com")
 '(smtpmail-smtp-service 25)
 '(vc-annotate-background "#1c1d20")
 '(vc-annotate-color-map
   (list
    (cons 20 "#78bd65")
    (cons 40 "#a4c756")
    (cons 60 "#d1d247")
    (cons 80 "#fedd38")
    (cons 100 "#f8bc33")
    (cons 120 "#f39b2e")
    (cons 140 "#ee7b29")
    (cons 160 "#dc7a54")
    (cons 180 "#ca797f")
    (cons 200 "#b978ab")
    (cons 220 "#c9648e")
    (cons 240 "#d95071")
    (cons 260 "#ea3d54")
    (cons 280 "#bf3e51")
    (cons 300 "#94404f")
    (cons 320 "#69424c")
    (cons 340 "#5B6268")
    (cons 360 "#5B6268")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(fixed-pitch ((t (:font #<font-spec nil nil DejaVu\ Sans\ Mono nil nil nil nil nil 18 nil nil nil nil>))))
 '(font-lock-comment-face ((t (:slant italic))))
 '(font-lock-keyword-face ((t (:slant italic))))
 '(org-document-info ((t (:height 1.33))))
 '(org-document-title ((t (:height 1.75))))
 '(org-level-1 ((t (:weight extra-bold :height 1.33))))
 '(org-level-2 ((t (:weight bold :height 1.25))))
 '(org-level-3 ((t (:weight bold :height 1.15))))
 '(org-level-4 ((t (:weight semi-bold :height 1.1))))
 '(org-level-5 ((t (:weight semi-bold :height 1.05))))
 '(org-level-6 ((t (:weight semi-bold :height 1.025))))
 '(org-level-8 ((t (:weight semi-bold))))
 '(org-level-9 ((t (:weight semi-bold))))
 '(variable-pitch ((t (:font #<font-spec nil nil DejaVu\ Sans nil nil nil nil nil 18 nil nil nil nil>)))))
