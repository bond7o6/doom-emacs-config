(setq user-full-name "Arlo Hobbs"
      user-mail-address "arlohobbs@hotmail.com")

(setq doom-font (font-spec :family "DejaVu Sans Mono" :size 18)
      doom-variable-pitch-font (font-spec :family "DejaVu Sans" :size 18)
      doom-big-font (font-spec :family "DejaVu Sans Mono" :size 36))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

(setq doom-theme 'doom-old-hope)

(require 'tramp)

(add-to-list 'backup-directory-alist
             (cons tramp-file-name-regexp nil))
(setq tramp-auto-save-directory "~/.emacs.d/.local/cache/tramp-autosave/")

(setq org-hide-emphasis-markers t)

(setq org-ellipsis " ▼")

;; (add-hook 'org-mode-hook '(org-mode . (eval . (org-indent-mode -1))))
;; (setq org-indent-indentation-per-level 0)
;; (org-mode . (eval . (org-indent-mode -1)))

;; (remove-hook 'org-mode-hook #'company-mode)

(setq org-startup-folded t)

;;

(setq org-directory "~/org/")

(require 'ox)

(use-package! ox-twbs
  :after ox)

;;(require 'ox-bibtex)

;;(setq bibtex2html-program "/bin/bibtex2html -nobibsource")

(setq org-latex-pdf-process
      '("pdflatex -interaction nonstopmode -output-directory %o %f"
        "bibtex %b"
        "bibtex %b"
        "pdflatex -interaction nonstopmode -output-directory %o %f"
        "pdflatex -interaction nonstopmode -output-directory %o %f"))

(add-to-list 'org-latex-classes
             '("assignment"
               "\\documentclass{/home/bond7o6/Templates/LaTeX/Assignment-ahobbs/assignment-ahobbs}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

;; (defvar org-latex-universal-preamble "
;; 
;; "
;;   "Preamble to be included in every export.")

;; (defvar org-latex-conditional-preambles
;;   `((t . org-latex-universal-preamble)
;;     ("\\[\\[file:.*\\.svg\\]\\]" . "\\usepackage{svg}"))
;;   "Snippets which are conditionally included in the preamble of a LaTeX export.

;; Alist where when the car results in a non-nil value, the cdr is inserted in
;; the preamble.  The car may be a:
;; - string, which is used as a regex search in the buffer
;; - symbol, the value of which used
;; - function, the result of the function is used

;; The cdr may be a:
;; - string, which is inserted without processing
;; - symbol, the value of which is inserted
;; - function, the result of which is inserted")

;; (defadvice! org-latex-header-smart-preamble (orig-fn tpl def-pkg pkg snippets-p &optional extra)
;;   "Dynamically insert preamble content based on `org-latex-conditional-preambles'."
;;   :around #'org-splice-latex-header
;;   (let ((header (funcall orig-fn tpl def-pkg pkg snippets-p extra)))
;;     (if snippets-p header
;;       (concat header
;;               (mapconcat (lambda (term-preamble)
;;                            (when (pcase (car term-preamble)
;;                                    ((pred stringp) (save-excursion
;;                                                        (goto-char (point-min))
;;                                                        (search-forward-regexp (car term-preamble) nil t)))
;;                                    ((pred functionp) (funcall (car term-preamble)))
;;                                    ((pred symbolp) (symbol-value (car term-preamble)))
;;                                    (_ (user-error "org-latex-conditional-preambles key %s unable to be used" (car term-preamble))))
;;                              (pcase (cdr term-preamble)
;;                                ((pred stringp) (cdr term-preamble))
;;                                ((pred functionp) (funcall (cdr term-preamble)))
;;                                ((pred symbolp) (symbol-value (cdr term-preamble)))
;;                                (_ (user-error "org-latex-conditional-preambles value %s unable to be used" (cdr term-preamble))))))
;;                          org-latex-conditional-preambles
;;                          "\n")))))

;; (use-package! engrave-faces-latex
;;   :after ox-latex)
;; (setq org-latex-listings 'engraved) ; NOTE non-standard value

(setq org-latex-listings 'minted
      org-latex-packages-alist '(("" "minted"))
      org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

;; (defadvice! org-latex-src-block-engraved (orig-fn src-block contents info)
;;   "Like `org-latex-src-block', but supporting an engraved backend"
;;   :around #'org-latex-src-block
;;   (if (eq 'engraved (plist-get info :latex-listings))
;;       (org-latex-scr-block--engraved src-block contents info)
;;     (funcall orig-fn src-block contents info)))

;; (defadvice! org-latex-inline-src-block-engraved (orig-fn inline-src-block contents info)
;;   "Like `org-latex-inline-src-block', but supporting an engraved backend"
;;   :around #'org-latex-inline-src-block
;;   (if (eq 'engraved (plist-get info :latex-listings))
;;       (org-latex-inline-scr-block--engraved inline-src-block contents info)
;;     (funcall orig-fn src-block contents info)))

;; (setq org-latex-engraved-code-preamble "
;; 
;; ")

;; (add-to-list 'org-latex-conditional-preambles '("#\\+BEGIN_SRC\\|#\\+begin_src" . org-latex-engraved-code-preamble) t)
;; (add-to-list 'org-latex-conditional-preambles '("#\\+BEGIN_SRC\\|#\\+begin_src" . engrave-faces-latex-gen-preamble) t)

;; (defun org-latex-scr-block--engraved (src-block contents info)
;;   (let* ((lang (org-element-property :language src-block))
;;          (attributes (org-export-read-attribute :attr_latex src-block))
;;          (float (plist-get attributes :float))
;;          (num-start (org-export-get-loc src-block info))
;;          (retain-labels (org-element-property :retain-labels src-block))
;;          (caption (org-element-property :caption src-block))
;;          (caption-above-p (org-latex--caption-above-p src-block info))
;;          (caption-str (org-latex--caption/label-string src-block info))
;;          (placement (or (org-unbracket-string "[" "]" (plist-get attributes :placement))
;;                         (plist-get info :latex-default-figure-position)))
;;          (float-env
;;           (cond
;;            ((string= "multicolumn" float)
;;             (format "\\begin{listing*}[%s]\n%s%%s\n%s\\end{listing*}"
;;                     placement
;;                     (if caption-above-p caption-str "")
;;                     (if caption-above-p "" caption-str)))
;;            (caption
;;             (format "\\begin{listing}[%s]\n%s%%s\n%s\\end{listing}"
;;                     placement
;;                     (if caption-above-p caption-str "")
;;                     (if caption-above-p "" caption-str)))
;;            ((string= "t" float)
;;             (concat (format "\\begin{listing}[%s]\n"
;;                             placement)
;;                     "%s\n\\end{listing}"))
;;            (t "%s")))
;;          (options (plist-get info :latex-minted-options))
;;          (content-buffer
;;           (with-temp-buffer
;;             (insert
;;              (let* ((code-info (org-export-unravel-code src-block))
;;                     (max-width
;;                      (apply 'max
;;                             (mapcar 'length
;;                                     (org-split-string (car code-info)
;;                                                       "\n")))))
;;                (org-export-format-code
;;                 (car code-info)
;;                 (lambda (loc _num ref)
;;                   (concat
;;                    loc
;;                    (when ref
;;                      ;; Ensure references are flushed to the right,
;;                      ;; separated with 6 spaces from the widest line
;;                      ;; of code.
;;                      (concat (make-string (+ (- max-width (length loc)) 6)
;;                                           ?\s)
;;                              (format "(%s)" ref)))))
;;                 nil (and retain-labels (cdr code-info)))))
;;             (funcall (org-src-get-lang-mode lang))
;;             (engrave-faces-latex-buffer)))
;;          (content
;;           (with-current-buffer content-buffer
;;             (buffer-string)))
;;          (body
;;           (format
;;            "\\begin{Code}\n\\begin{Verbatim}[%s]\n%s\\end{Verbatim}\n\\end{Code}"
;;            ;; Options.
;;            (concat
;;             (org-latex--make-option-string
;;              (if (or (not num-start) (assoc "linenos" options))
;;                  options
;;                (append
;;                 `(("linenos")
;;                   ("firstnumber" ,(number-to-string (1+ num-start))))
;;                 options)))
;;             (let ((local-options (plist-get attributes :options)))
;;               (and local-options (concat "," local-options))))
;;            content)))
;;     (kill-buffer content-buffer)
;;     ;; Return value.
;;     (format float-env body)))

;; (defun org-latex-inline-scr-block--engraved (inline-src-block _contents info)
;;   (let ((options (org-latex--make-option-string
;;                   (plist-get info :latex-minted-options)))
;;         code-buffer code)
;;     (setq code-buffer
;;           (with-temp-buffer
;;             (insert (org-element-property :value inline-src-block))
;;             (funcall (org-src-get-lang-mode
;;                       (org-element-property :language inline-src-block)))
;;             (engrave-faces-latex-buffer)))
;;     (setq code (with-current-buffer code-buffer
;;                  (buffer-string)))
;;     (kill-buffer code-buffer)
;;     (format "\\Verb%s{%s}"
;;             (if (string= options "") ""
;;               (format "[%s]" options))
;;             code)))

;; (defadvice! org-latex-example-block-engraved (orig-fn example-block contents info)
;;   "Like `org-latex-example-block', but supporting an engraved backend"
;;   :around #'org-latex-example-block
;;   (let ((output-block (funcall orig-fn example-block contents info)))
;;     (if (eq 'engraved (plist-get info :latex-listings))
;;         (format "\\begin{Code}[alt]\n%s\n\\end{Code}" output-block)
;;       output-block)))

(eval-after-load 'ox '(require 'ox-koma-letter))
(eval-after-load 'ox-koma-letter
  '(progn
     (add-to-list 'org-latex-classes
                  '("basic-letter"
                    "\\documentclass\[12pt, fromalign=right\]{scrlttr2}"))
     (setq org-koma-letter-default-class "basic-letter")))

(setq org-publish-project-alist
      '(("local-ah2001-org"
         :base-directory "~/org/hwu_ah2001/source/"
         :publishing-directory "~/org/hwu_ah2001/public_html/"
         :base-extension "org"
         :with-toc nil
         :section-numbers nil
         :index nil
         :auto-sitemap t
         :html-home/up-format "<div id=\"org-div-home-and-up\">\n <a accesskey=\"H\" href=\"./index.html\"> Home </a>\n |\n <a accesskey=\"h\" href=\"./about.html\"> About </a>\n |\n <a accesskey=\"h\" href=\"./sitemap.html\"> Sitemap </a>\n </div>"
         :html-link-home "./index.html"
         :with-email t
         :with-creator t
         :sitemap-file-entry-format "%t%a%d"
         :publishing-function org-html-publish-to-html
         :html-head "<link rel=\"stylesheet\"
                      type=\"text/css\"
                      href=\"https://gongzhitaao.org/orgcss/org.css\"/>"
         :html-preamble t)
        ("local-ah2001-attach"
         :base-directory "~/org/hwu_ah2001/source/"
         :publishing-directory "~/org/hwu_ah2001/public_html/"
         :base-extension "css\\|png\\|jpg\\|gif\\|pdf"
         :recursive t
         :publishing-function org-publish-attachment)
        ("local-ah2001"
         :components ("local-ah2001-org" "local-ah2001-attach"))
        ("hwu-ah2001-org"
         :base-directory "~/org/hwu_ah2001/source/"
         :publishing-directory "/ssh:ah2001@ssh.macs.hw.ac.uk:~/public_html/"
         :base-extension "org"
         :with-toc nil
         :section-numbers nil
         :index nil
         :auto-sitemap t
         :html-home/up-format "<div id=\"org-div-home-and-up\">\n <a accesskey=\"H\" href=\"./index.html\"> Home </a>\n |\n <a accesskey=\"h\" href=\"./about.html\"> About </a>\n |\n <a accesskey=\"h\" href=\"./sitemap.html\"> Sitemap </a>\n </div>"
         :html-link-home "./index.html"
         :with-email t
         :with-creator t
         :sitemap-file-entry-format "%t%a%d"
         :publishing-function org-html-publish-to-html
         :html-head "<link rel=\"stylesheet\"
                      type=\"text/css\"
                      href=\"https://gongzhitaao.org/orgcss/org.css\"/>"
         :html-preamble t
         :exclude "test.org")
        ("hwu-ah2001-attach"
         :base-directory "~/org/hwu_ah2001/source/"
         :publishing-directory "/ssh:ah2001@ssh.macs.hw.ac.uk:~/public_html/"
         :base-extension "css\\|png\\|jpg\\|gif\\|pdf"
         :recursive t
         :publishing-function org-publish-attachment)
        ("hwu-ah2001"
         :components ("hwu-ah2001-org" "hwu-ah2001-attach"))))

(setq org-roam-dailies-directory (concat org-directory "/daily/"))

(add-hook 'after-init-hook 'org-roam-mode)

(use-package! org-roam-bibtex
  :after (org-roam)
  :hook (org-roam-mode . org-roam-bibtex-mode)
  :config
  (setq orb-preformat-keywords
        '("=key=" "title" "url" "file" "author-or-editor" "keywords"))
  (setq orb-templates
        '(("r" "ref" plain (function org-roam-capture--get-point)
           ""
           :file-name "${slug}"
           :head "#+TITLE: ${=key=}: ${title}\n#+ROAM_KEY: ${ref}
- tags ::
- keywords :: ${keywords}
\n* ${title}\n  :PROPERTIES:\n  :Custom_ID: ${=key=}\n  :URL: ${url}\n  :AUTHOR: ${author-or-editor}\n  :NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n  :NOTER_PAGE: \n  :END:\n\n"
           :unnarrowed t))))

(setq
 bibtex-completion-notes-path "~/org/roam/"
 bibtex-completion-bibliography "~/Documents/Refs/master.bib"
 bibtex-completion-pdf-field "file"
 bibtex-completion-notes-template-multiple-files
 (concat
  "#+TITLE: ${title}\n"
  "#+ROAM_KEY: cite:${=key=}\n"
  "* TODO Notes\n"
  ":PROPERTIES:\n"
  ":Custom_ID: ${=key=}\n"
  ":NOTER_DOCUMENT: %(orb-process-file-field \"${=key=}\")\n"
  ":AUTHOR: ${author-abbrev}\n"
  ":JOURNAL: ${journaltitle}\n"
  ":DATE: ${date}\n"
  ":YEAR: ${year}\n"
  ":DOI: ${doi}\n"
  ":URL: ${url}\n"
  ":END:\n\n"))

(use-package! org-ref
  :after org
  :config
  (setq
   org-ref-completion-library 'org-ref-ivy-cite
   org-ref-get-pdf-filename-function 'org-ref-get-pdf-filename-helm-bibtex
   org-ref-default-bibliography (list "~/Documents/Refs/master.bib")
   org-ref-bibliography-notes "~/org/roam/bibnotes.org"
   org-ref-note-title-format "* TODO %y - %t\n :PROPERTIES:\n  :Custom_ID: %k\n  :NOTER_DOCUMENT: %F\n :ROAM_KEY: cite:%k\n  :AUTHOR: %9a\n  :JOURNAL: %j\n  :YEAR: %y\n  :VOLUME: %v\n  :PAGES: %p\n  :DOI: %D\n  :URL: %U\n :END:\n\n"
   org-ref-notes-directory "~/org/roam/"
   org-ref-notes-function 'orb-edit-notes))

(setq org-preview-latex-default-process 'dvisvgm)

;;(plist-put org-format-latex-options :scale 1.2)

(setq org-startup-with-latex-preview nil)

;;(defun usd-ren-hax ()
;;  (when (looking-back (rx "$ "))
;;    (save-excursion
;;      (backward-char 1)
;;      (org-latex-preview))))
;;
;;(add-hook 'org-mode-hook
;;          (lambda ()
;;            (org-cdlatex-mode)
;;            (add-hook 'post-self-insert-hook #'usd-ren-hax 'append 'local)))
;;
;;(defun iln-ren-hax ()
;;  (when (looking-back (rx "\) "))
;;    (save-excursion
;;      (backward-char 2)
;;      (org-latex-preview)
;;      (forward-char 2))))
;;
;;(add-hook 'org-mode-hook
;;          (lambda ()
;;            (org-cdlatex-mode)
;;            (add-hook 'post-self-insert-hook #'iln-ren-hax 'append 'local)))
;;
;;(defun equ-ren-hax ()
;;  (when (looking-back (rx "\] "))
;;    (save-excursion
;;      (backward-char 2)
;;      (org-latex-preview)
;;      (forward-char 2))))
;;
;;(add-hook 'org-mode-hook
;;          (lambda ()
;;            (org-cdlatex-mode)
;;            (add-hook 'post-self-insert-hook #'equ-ren-hax 'append 'local)))

;;(defun usd-hack ()
;;  (when (looking-back (rx "$"))
;;    (save-excursion
;;      (insert-char #x24)
;;      (backward-char 1))))
;;
;;(add-hook 'org-mode-hook
;;          (lambda ()
;;            (add-hook 'post-self-insert-hook #'usd-hack 'append 'local)))

(setq org-tags-column '60)

(setq org-log-done 'time)

(setq org-default-notes-file (concat org-directory "/notes.org"))

(add-to-list 'org-capture-templates
             '("e" "Emacs/Tech Todos"))
(add-to-list 'org-capture-templates
             '("ee" "General Emacs" entry (file+olp "~/org/todo.org" "Emacs" ":Emacs:")
               "*** TODO %?\n%u"))
(add-to-list 'org-capture-templates
             '("em" "Emacs Modes" entry (file+olp "~/org/todo.org" "Emacs" ":Emacs-Modes:")
               "*** TODO %?\n%u"))
(add-to-list 'org-capture-templates
             '("ea" "Assorted" entry (file+olp "~/org/todo.org" "Emacs" ":Tech-Assort:")
               "*** TODO %?\n%u"))

(add-to-list 'org-capture-templates
             '("b" "Purchases"))
(add-to-list 'org-capture-templates
             '("bu" "Urgent" entry (file+olp "~/org/purchase.org" "Urgent")
               "** TODO %^{PROMPT} \nDEADLINE: %^t\n-  %?"))
(add-to-list 'org-capture-templates
             '("ba" "Assorted" entry (file+olp "~/org/purchase.org" "Assorted")
               "** %^{PROMPT}\n%u\n-  %?"))
(add-to-list 'org-capture-templates
             '("bf" "Clothes/Fashion" entry (file+olp "~/org/purchase.org" "Fashion")
               "** %^{PROMPT}\n%u\n-  %?"))
(add-to-list 'org-capture-templates
             '("bs" "EDC+Stationary" entry (file+olp "~/org/purchase.org" "Small Items (EDC+Stationary)")
               "** %^{PROMPT}\n%u\n-  %?"))
(add-to-list 'org-capture-templates
             '("bt" "Tea" entry (file+olp "~/org/purchase.org" "Tea")
               "** %^{PROMPT}\n%u\n-  %?"))

(setq org-archive-location (concat org-directory "/archive.org::* FROM %s"))

(require 'org-element)

(use-package! org-chef
  :commands (org-chef-insert-recipe org-chef-get-recipe-from-url))

(setq doom-modeline-enable-word-count t)

(setq display-line-numbers-type 'relative)

(global-display-line-numbers-mode t)

(setq fill-column 80
      display-fill-column-indicator-column t)

(global-display-fill-column-indicator-mode t)

(global-visual-line-mode t)

(map! :leader
      :desc "Calc"
      "o c" #'calc
      :desc "Calendar"
      "o C" #'cfw:open-org-calendar
      :desc "Email/Mu4e"
      "o m" #'mu4e
      :desc "Elfeed"
      "o C-f" #'elfeed)

(map! :leader
      :desc "other-window"
      "w SPC w" #'other-window)

(map! :leader
      (:prefix ("y" . "yank")
       :desc "Yank hinted link" "f" #'link-hint-copy-link))

(show-paren-mode 1)

(set-frame-parameter (selected-frame) 'alpha 96)
(add-to-list 'default-frame-alist '(alpha . (96 . 96)))

(add-hook 'window-setup-hook #'toggle-frame-fullscreen)
(add-hook 'window-setup-hook #'toggle-frame-maximized)

(setq auto-save-default t
      make-backup-files t)

(setq reftex-default-bibliography "~/Documents/Refs/master.bib")

(setq lsp-latex-texlab-executable "/bin/texlab")

;;(require 'lsp-latex)

;;(with-eval-after-load "tex-mode"
;;  (add-hook 'tex-mode-hook 'lsp)
;;  (add-hook 'latex-mode-hook 'lsp))

;;(with-eval-after-load "bibtex"
;;  (add-hook 'bibtex-mode-hook 'lsp))

;;(setq lsp-headerline-breadcrumb-enable t)

;;(setq lsp-ui-sideline-enable t)

;;(setq calc-angle-mode ’rad ;; radians are rad
;;      calc-algebraic-mode t ;; allows ’2*x instead of ’x<RET>2*
;;      calc-symbolic-mode t) ;; keeps stuff like √2 irrational for as long as possible
;;(after! calctex (setq calctex-format-latex-header (concat calctex-format-latex-header “\n\usepackage{arevmath}”)))

;;(add-hook 'calc-mode-hook #'calctex-mode)

(setq rmh-elfeed-org-files (list "~/org/elfeed.org"))

(after! elfeed
  (setq-default elfeed-search-filter "@1-week-ago +unread -ao3 -reddit "))

(defun elfeed-search-format-date (date)
  (format-time-string "%Y-%m-%d %H:%M" (seconds-to-time date)))

;;(setq european-calendar-style t)

;;(define-key )

(setq calendar-week-start-day 1)

;;(add-to-list 'load-path "")


;; Each path is relative to `+mu4e-mu4e-mail-path', which is ~/.mail by default
;;
;;
;;(after! org-msg (org-msg-mode -1))
;;
;;(setq mu4e-maildir (expand-file-name "~/sdb-2TB/Maildir"))
;;
;;(setq mu4e-get-mail-command "mbsync -c ~/.mail/.mbsyncrc -a"
;;      mu4e-update-interval 300
;;      message-send-mail-function 'smtpmail-send-it
;;      starttls-use-gnutls t)

(require 'mu4e)
(add-to-list 'load-path "/usr/share/emacs/site-lisp/mu4e")

(setq message-send-mail-function 'smtpmail-send-it
      starttls-use-gnutls t)

(setq smtpmail-debug-info t)

;;(setq smtpmail-starttls-credentials
;;      '(("smtp.office365.com" 465 nil nil)))

;; Set to 't' to avoid mail syncing issues when using mbsync
(setq mu4e-change-filenames-when-moving t)

(setq gnutls-verify-error t)

;; Refresh mail using isync every 10 minutes
(setq mu4e-update-interval (* 10 60))
(setq mu4e-get-mail-command "mbsync -a")
(setq mu4e-maildir "~/sdb-2TB/Maildir")

(setq mu4e-contexts
      `( ,(make-mu4e-context ;;Univeristy
           :name "Heriot-Watt"
           :match-func
           (lambda (msg)
             (when msg
               (string-prefix-p "/hw" (mu4e-message-field msg :maildir))))
           :vars '((user-mail-address . "ah2001@hw.ac.uk")
                   ;;(mu4e-compose-reply-to-address . "ah2001@hw.ac.uk")
                   (user-full-name . "Arlo Hobbs")
                   (mu4e-compose-signature . "Regards,\nArlo Hobbs\n---")
                   (smtpmail-starttls-credentials . '(("smtp.office365.com" 587 nil nil)))
                   (smtpmail-smtp-server . "smtp.office365.com")
                   (smtpmail-smtp-service . 587)
                   (smtpmail-stream-type . starttls)
                   (mu4e-drafts-folder . "/hw/Drafts")
                   (mu4e-sent-folder . "/hw/Sent Items")
                   (mu4e-refile-folder . "/hw/Archive")
                   (mu4e-trash-folder . "/hw/Deleted Items")))
         ;; Arlo @ hotmail
         ,(make-mu4e-context
           :name "Arlo@hot"
           :match-func
           (lambda (msg)
             (when msg
               (string-prefix-p "/arlo-hotmail" (mu4e-message-field msg :maildir))))
           :vars '((user-mail-address . "arlohobbs@hotmail.com")
                   ;;(mu4e-compose-reply-to-address . "arlohobbs@hotmail.com")
                   (user-full-name . "Arlo Hobbs")
                   (mu4e-compose-signature . "Regards,\nArlo Hobbs\n---")
                   (smtpmail-starttls-credentials . '(("smtp.office365.com" 587 nil nil)))
                   (smtpmail-smtp-server . "smtp.office365.com")
                   (smtpmail-smtp-service . 587)
                   (smtpmail-stream-type . nil)
                   (mu4e-drafts-folder . "/arlo-hotmail/Drafts")
                   (mu4e-sent-folder . "/arlo-hotmail/Sent Items")
                   (mu4e-refile-folder . "/arlo-hotmail/Archive")
                   (mu4e-trash-folder . "/arlo-hotmail/Deleted Items")))
         ;; Bond @ hotmail
         ,(make-mu4e-context
           :name "Bond@hot"
           :match-func
           (lambda (msg)
             (when msg
               (string-prefix-p "/bond-hotmail" (mu4e-message-field msg :maildir))))
           :vars '((user-mail-address . "bond7o6@hotmail.com")
                   ;;(mu4e-compose-reply-to-address . "bond7o6@hotmail.com")
                   (user-full-name . "Arlo Hobbs")
                   (mu4e-compose-signature . "Regards,\nArlo Hobbs\n---")
                   (smtpmail-starttls-credentials . '(("smtp.office365.com" 587 nil nil)))
                   (smtpmail-smtp-server . "smtp.office365.com")
                   (smtpmail-smtp-service . 587)
                   (smtpmail-stream-type . nil)
                   (mu4e-drafts-folder . "/bond-hotmail/Drafts")
                   (mu4e-sent-folder . "/bond-hotmail/Sent Items")
                   (mu4e-refile-folder . "/bond-hotmail/Archive")
                   (mu4e-trash-folder . "/bond-hotmail/Deleted Items")))))

(add-to-list 'load-path "/home/bond7o6/.emacs.d/.local/straight/repos/java-one-click-run/java-one-click-run.el") (require 'java-one-click-run)

(use-package! java-one-click-run
  :init (package! shell-here)
  :bind ("<f5>" . java-one-click-run))

(setq screenshot-line-numbers-p t
      screenshot-min-width 60
      screenshot-max-width 140)

(global-set-key (kbd "<f9>") 'gif-screencast-start-or-stop)

(use-package! gif-screencast
  :commands gif-screencast-mode
  :config
  (setq gif-screencast-program "maim"
        gif-screencast-args `("--quality" "3" "-i" ,(string-trim-right
                                                     (shell-command-to-string
                                                      "xdotool getactivewindow")))
        gif-screencast-optimize-args '("--batch" "--optimize=3" "--usecolormap=/tmp/doom-color-theme"))
  (defun gif-screencast-write-colormap ()
    (f-write-text
     (replace-regexp-in-string
      "\n+" "\n"
      (mapconcat (lambda (c) (if (listp (cdr c))
                                 (cadr c))) doom-themes--colors "\n"))
     'utf-8
     "/tmp/doom-color-theme" ))
  (gif-screencast-write-colormap)
  (add-hook 'doom-load-theme-hook #'gif-screencast-write-colormap))

(use-package! keycast
  :commands keycast-mode
  :config
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line."
    :global t
    (if keycast-mode
        (progn
          (add-hook 'pre-command-hook 'keycast--update t)
          (add-to-list 'global-mode-string '("" mode-line-keycast " ")))
      (remove-hook 'pre-command-hook 'keycast--update)
      (setq global-mode-string (remove '("" mode-line-keycast " ") global-mode-string))))
  (custom-set-faces!
    '(keycast-command :inherit doom-modeline-debug
                      :height 0.9)
    '(keycast-key :inherit custom-modified
                  :height 1.1
                  :weight bold)))

;; (use-package! forge
;;   :after magit)

(add-hook 'sql-interactive-mode-hook
          (lambda ()
            (toggle-truncate-lines t)))
