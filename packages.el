;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)


(package! magit-section)
(package! paredit :recipe (:depth full))


;; Tree-sitter
;; (package! tree-sitter)
;; (package! tree-sitter-lang)

;;Export org-mode to twitter bootstrap html
(package! ox-twbs)

;; refs in org-mode? not sure it's setup right
(package! org-ref)

;; deal with reference through org-roam
(package! org-roam-bibtex
  :recipe (:host github
           :repo "org-roam/org-roam-bibtex"))

;; When using org-roam via the `+roam` flag
;;(unpin! org-roam company-org-roam)

;; When using bibtex-completion via the `biblio` module
(unpin! bibtex-completion helm-bibtex ivy-bibtex)

;; Pretty code screenshots
(package! screenshot
  :recipe (:host github
           :repo "tecosaur/screenshot")
  :pin "f8204e82dc0c1158c401735d36a143e6f6d24cf5")

;; Pretty code highlighting on export
(package! engrave-faces
  :recipe (:host github
           :repo "tecosaur/engrave-faces"))

;; For reading ebooks
(package! nov :recipe (:depth full))
(package! calibredb)

;; Make doc buffers look nicer
(package! info-colors)

;; ;; theme magic for the terminal
;; (package! theme-magic)

;; calc-mode with latex, however it hangs emacs...
(package! calctex :recipe (:host github :repo "johnbcoughlin/calctex"
                           :files ("*.el" "calctex/*.el" "calctex-contrib/*.el" "org-calctex/*.el" "vendor"))
  :pin "784cf911bc96aac0f47d529e8cee96ebd7cc31c9")

;; grep pdfs, it is useful
(package! pdfgrep)

;; Display keyboard layout
(package! dkl)

;;; Accounting
(package! dklrt)
(package! flycheck-hledger)
(package! hledger-mode)

(package! lsp-ltex)
(package! flycheck-languagetool)
;; (package! flymake-proselint)
(package! github)

(package! pomm)

(package! mode-line-timer
  :recipe (:host github
           :repo "syohex/emacs-mode-line-timer"))

(package! stopwatch
  :recipe (:host github
           :repo "blue0513/stopwatch"))

(package! tldr)

;; Mu4e handles email within emacs. I don't like sending HTML email so I haven't
;; configured org-msg, which I can't figure out how to disable with Doom Emacs's
;; Mu4e implementation, hence installing it here instead of its module.
;;(package! mu4e)

;; (package! shell-here)

;;(package! lsp-latex)          ;;lsp for latex :)

;; Page break prettier
(package! page-break-lines
  :recipe (:host github
           :repo "purcell/page-break-lines"))

;; (package! dirvish) ;; TODO seems broke, need to look into

(package! mu4e-alert)

(package! cell-mode
  :recipe (:host gitlab
           :repo "dto/cell-mode"))

;; org-tree-slide
(package! org-tree-slide)


;; ESS goodness
(package! ess-view)

;; Speed-reading mode
(package! spray)

(package! sx)

;; display keys pressed
(package! keycast)

;; Make GIFs
(package! gif-screencast)

;; Buffer with all previous commands and key presses
(package! command-log-mode)

;; bluetooth management
(package! bluetooth)

;; Play Go via GnuGo
(package! gnugo)

(package! eperiodic)

;; shrface
(package! shrface)

;; Anki
(package! anki
  :recipe (:host github
           :repo "chenyanming/anki.el"))

;; sagemath - CAS
(package! sage-shell-mode)

;; ;; winum
;; (package! winum)

;; pacman
(package! pacmacs)

;; Discord integration
(package! accord
  :recipe (:host github
           :repo "progfolio/accord"))

;; Dash
;; (package! dash-docs)

;; Forge to manage github + gitlab + etc. in emacs
;; (package! forge)

;; EAF (emacs application framework) + dependencies
;; Dependencies
(package! s)
(package! epc)
(package! deferred)
(package! ctable)

(package! eaf
  :recipe (:host github
           :repo "emacs-eaf/emacs-application-framework"
           :files ("*")))

(package! eaf-browser
  :recipe (:host github
           :repo "emacs-eaf/eaf-browser"
           :files ("*")))

(package! eaf-system-monitor
  :recipe (:host github
           :repo "emacs-eaf/eaf-system-monitor"
           :files ("*")))

(package! eaf-demo
  :recipe (:host github
           :repo "emacs-eaf/eaf-demo"
           :files ("*")))

(package! eaf-video-player
  :recipe (:host github
           :repo "emacs-eaf/eaf-video-player"
           :files ("*")))
;; (package! eaf
;;   :recipe (:host github
;;            :repo "manateelazycat/emacs-application-framework"
;;            :files ("*")))

;; ARM-mode for ARM assembly
(package! arm-mode
  :recipe (:host github
           :repo "charje/arm-mode"))

;; Org-indent manages the cosmetic indentation of sections and such-like
;; (package! org-indent
;;   :disable t)

;; view devdocs.os
(package! devdocs-browser
  :recipe (:host github
           :repo "blahgeek/emacs-devdocs-browser"))

;;; Media Players

;; lastfm.el - dependency of Vuiet
(package! lastfm)

;; Vuiet
;; Play music off youtube
(package! vuiet)

;; simple-mpc
;; Frontend for mpc/mpd (I don't really understand this)
(package! simple-mpc)

;; doom-modeline-now-playing
(package! doom-modeline-now-playing)

;; alert
;; (package! alert)

;;;

(package! subr-x
  :recipe (:local-repo "subr-x"))

;; Org-outline view
;; It is really cool
(package! org-ol-tree
  :recipe (:host github
           :repo "Townk/org-ol-tree"))

;; centered window
(package! centered-window)

;;; Some fun frontends

;; View reddit in org-mode
(package! reddigg
  :recipe (:host github
           :repo "thanhvg/emacs-reddigg"))

(package! nnreddit)
(package! nndiscourse)
(package! nnhackernews)

;; View HackerNews in org-mode
;; (package! hnreader
;;   :recipe (:host github
;;            :repo "thanhvg/emacs-hnreader"))

;; Full reddit instance in emacs
;; (package! md4rd
;;   :recipe (:host github
;;            :repo "ahungry/md4rd"))

;; chembalance
(package! chembalance
  :recipe (:host github
           :repo "sergiruiztrepat/chembalance"))

;; wikipedia
(package! wiki-summary)

;; kiwix to get better wikipedia
(package! kiwix)

;; Maxima intergration
(package! maxima
  :recipe (:host gitlab
           :repo "sasanidas/maxima"))

(package! diss
  :recipe (:host github
           :repo "ieure/diss"))

(package! edbi)

(package! edbi-sqlite)

(package! lexic)

(package! simple-timers
  :recipe (:local-repo "lisp/simple-timers"))

(package! doct)

(package! sdcv)

(package! beancount
  :recipe (:host github
           :repo "beancount/beancount-mode"))

;;; Games

;; 2048
(package! 2048-game)

;; sudoku
(package! sudoku)

;; steam
(package! steam)

;; mine-sweeper
(package! minesweeper)

;; chess
(package! chess)

;; tetris
(package! autotetris-mode)

;; Mentor
(package! mentor
  :recipe (:host github
           :repo "skangas/mentor"))

;; wallabag
(package! wallabag
  :recipe (:host github
           :repo "chenyanming/wallabag.el"
           :files ("*")))

;; manage packages
(package! helm-system-packages)

;; plz
(package! plz
  :recipe (:host github
           :repo "alphapapa/plz.el"))

;; ement.el for matrix
(package! ement
  :recipe (:host github
           :repo "alphapapa/ement.el"))

;; Edit text boxes in firefox in emacs
(package! atomic-chrome)


;; Interact with firefox marionette
(package! marionette
  :recipe (:host github
           :repo "xuchunyang/marionette.el"))

(package! ox-gemini)
(package! elpher)
(package! gemini-mode)
(package! gemini-write)


;;; Typing

;; monkeytype
(package! monkeytype)

;; speed-type
(package! speed-type)

;; typit
(package! typit)

(package! w3m)

(package! empv
 :recipe (:host github
           :repo "isamert/empv.el"))

(package! mu4e-column-faces)


;; Try to get notifications from org-agenda
;; (package! org-alert)
;; (package! org-notifications)
(package! org-timed-alerts
  :recipe (:host github
           :repo "legalnonsense/org-timed-alerts"))


(package! org-ql)



;;; Here Be Dragons!
;; (package! xelb)
;; (package! exwm)
;; (package! exwm-firefox-core)
;; (package! exwm-firefox-evil)
;; (package! exwm-edit)
;; (package! exwm-float)
